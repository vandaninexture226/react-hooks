import React, { Component } from 'react';
import './App.css';
import Child from './child';

class App extends Component{
  constructor(){
    super();
    this.state = {
      name: 'john'
    };

    console.log('constructor');
  }

  componentWillMount(){
    if(window.innerWidth < 500){
      this.setState({innerWidth:window.innerWidth});
    }
    console.log('componentWillMount');
  }

  componentDidMount(){
    console.log('componentDidMount');
  }

  shouldComponentUpdate(nextProps, nextState){
    console.log('shouldComponentUpdate')
    return true;
  }

  componentWillUpdate(){
    console.log('componentWillUpdate')
  }

  changeState(){
    this.setState({name: 'jill'});
  }

  render(){
    console.log('render');
    return (
      <div className="App">
        name: {this.state.name}
          innerWidth: {this.state.innerWidth}
          <Child name={this.state.name}></Child>
          <button onClick={this.changeState.bind(this)}>Change State</button>
      </div>
    );
  }
}

export default App;
