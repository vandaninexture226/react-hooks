import React, { Component } from 'react';
import './App.css';

class Child extends Component{
  constructor(){
    super();
    console.log('child constructor');
  }

  componentWillMount(){
    console.log('child componentWillMount');
  }

  componentDidMount(){
    console.log('child componentDidMount');
  }

  componentWillReceiveProps(){
    console.log('child componentWillReceiveProps');
  }

  shouldComponentUpdate(nextProps, nextState){
      console.log('child shouldComponentUpdate')
      return false;
  }

  render(){
    console.log('render');
    return (
      <div className="App">
        child name: {this.props.name}
      </div>
    );
  }
}

export default Child;
